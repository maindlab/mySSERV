
import com.firefly.kotlin.ext.common.firefly

    fun main(args: Array<String>){
        val client = firefly.createWebSocketClient()
        client.webSocket("ws://localhost:4343/opensoket")
                .onText { text, _ -> println("The client received: $text") }
                .connect()
                .thenAccept { conn -> conn.sendText("Hello server.") }
    }

